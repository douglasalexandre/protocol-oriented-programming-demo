//
//  ImageWithSwitchTableViewCell.swift
//  POPDemo
//
//  Created by Douglas Taquary on 07/06/16.
//  Copyright © 2016 Douglas Taquary. All rights reserved.
//

import UIKit

typealias ImageWithSwitchViewPresentable = protocol<TextPresentable, TogglePresentable, ImagePresentable>

class ImageWithSwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var toggleSwitch: UISwitch!
    
    func configure(presenter: ImageWithSwitchViewPresentable) {
        
        titleText.text = presenter.text
        
        toggleSwitch.on = presenter.toggleOn
        toggleSwitch.onTintColor = presenter.toggleColor
        iconView.image = UIImage(named: presenter.imageName)
    }
}


