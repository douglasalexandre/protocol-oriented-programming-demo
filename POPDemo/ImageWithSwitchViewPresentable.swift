//
//  ImageWithSwitchViewPresentable.swift
//  POPDemo
//
//  Created by Douglas Taquary on 07/06/16.
//  Copyright © 2016 Douglas Taquary. All rights reserved.
//

import UIKit

//MARK: Protocolos
protocol TextPresentable {
    var text: String { get }
    var textColor: UIColor { get }
    var textFont: UIFont { get }
}

protocol TogglePresentable {
    var toggleOn: Bool { get }
    var toggleColor: UIColor { get }
    //func toggleOnTogleOn(on: Bool)
}

protocol ImagePresentable {
    var imageName: String { get }
}