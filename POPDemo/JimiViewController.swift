//
//  JimiViewController.swift
//  POPDemo
//
//  Created by Douglas Taquary on 07/06/16.
//  Copyright © 2016 Douglas Taquary. All rights reserved.
//

import UIKit

class JimiViewController: UITableViewController {
    
    enum Artist: Int {
        case JimiHendrix
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Table view data resource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let artist = Artist(rawValue: indexPath.row) {
            switch artist {
            case .JimiHendrix:
                let cell = tableView.dequeueReusableCellWithIdentifier("imageWithSwitchCell", forIndexPath: indexPath) as! ImageWithSwitchTableViewCell
                
                let viewModel = JimiViewModel()
                cell.configure(viewModel)
                return cell
            }
        }
        
        return tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
    }
}


