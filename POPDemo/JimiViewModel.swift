
//  Created by Douglas Taquary on 07/06/16.
//  Copyright © 2016 Douglas Taquary. All rights reserved.

import UIKit

struct JimiViewModel: ImageWithSwitchViewPresentable {
    //Here implementation handler parameters
    //In case our app are static data
}

// MARK: TextPresentable
extension JimiViewModel {
    var text: String { return "Jimi Hendrix"}
    var textColor: UIColor { return .yellowColor() }
    var textFont: UIFont { return .systemFontOfSize(16.0) }
}

// MARK: TogglePresentable
extension JimiViewModel {
    var toggleOn: Bool { return false }
    var toggleColor: UIColor { return .redColor() }
    
    func toggleOnTogleOn(on: Bool) {
        if on {
            print("Jimi está tocando Little wing")
        }
        else {
            print("Jimi parou de tocar")
        }
    }
}
//MARK: ImagePresentable
extension JimiViewModel {
    var imageName: String { return "jimi_hendrix"}
}




